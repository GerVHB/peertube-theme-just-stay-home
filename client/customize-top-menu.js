function register ({ registerHook, peertubeHelpers }) {

    document.querySelector("footer").innerHTML = "<a href='https://juststayhome.co' " +
                                                            "rel='noopener noreferrer' " +
                                                            "target='_blank' " +
                                                            "title='JustStayHome.Co website'>Powered by JustStayHome.Co" +
                                                          "</a>";
    var aboutLink = document.querySelector("a[href='/about']");
    var panelBlock = aboutLink.parentElement;
    var categoriesLinks = "<a routerlink='/search?categoryOneOf=11&sort=-match' routerlinkactive='active' href='/search?categoryOneOf=11&sort=-match'>" +
                            "Just Stay Home Contest" +
                          "</a>";
    aboutLink.remove();
    panelBlock.querySelector('.block-title').innerHTML = "Categories";
    panelBlock.innerHTML += categoriesLinks;
}

export {
    register
}
